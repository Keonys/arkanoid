#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

struct { double x; double y;  double vx; double vy; } ball;
/*MAP OF THE CURRENT LEVEL*/
char listBrick[741];

/*TIMERS*/
Uint64 prev, now;

/*TIME DURATION IN MS*/
double delta_t;

int x_vault, vie = 3;
bool start, quit;

SDL_Window* pWindow = NULL;
SDL_Surface* win_surf = NULL;
SDL_Surface* plancheSprites = NULL;

SDL_Rect srcBg = { 128, 128, 63, 52 };
SDL_Rect srcBall = { 46, 67, 17, 11 };
SDL_Rect srcVaiss = { 384, 192, 97, 15 };
SDL_Rect srcBrick = { 31, 15, 32, 16 };
SDL_Rect srcVie = { 0, 318, 30, 30 };

/*COLORED BRICKS*/
SDL_Rect srcBrick_White = { 0, 0, 31, 15 };
SDL_Rect srcBrick_Orange = { 31, 0, 32, 15 };
SDL_Rect srcBrick_Cyan = { 63, 0, 32, 15 };
SDL_Rect srcBrick_Green = { 95, 0, 32, 15 };
SDL_Rect srcBrick_DarkBlue = { 127, 0, 32, 15 };
SDL_Rect srcBrick_DarkGreen = { 159, 0, 32, 15 };
SDL_Rect srcBrick_Red = { 0, 15, 31, 16 };
SDL_Rect srcBrick_Blue = { 31, 15, 32, 16 };
SDL_Rect srcBrick_Magenta = { 63, 15, 32, 16 };
SDL_Rect srcBrick_Yellow = { 95, 15, 32, 16 };
SDL_Rect srcBrick_Brown = { 127, 15, 32, 16 };
SDL_Rect srcBrick_BlueGreen = { 159, 15, 32, 16 };

int initMap(int level);
void init();
void initBall();
void draw();
void initBall();

int main(int argc, char** argv)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0 )
    {
        /*GET THE ERROR OF INITIALIZATION*/
		fprintf(stderr, "Erreur SDL_Init : %s", SDL_GetError());
		SDL_Quit();
        return EXIT_FAILURE;
    }

	init();

	quit = false;
	int x_prev = 0;
	while (!quit)
	{
		SDL_Event event;
		while (!quit && SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
					/*ESCAPE KEY*/
					case SDLK_ESCAPE: quit = true; break;
					default: break;
				}
				break;
			case SDL_MOUSEMOTION:
			    x_prev = x_vault + event.motion.xrel;
			    if(x_prev > 0 && x_prev + srcVaiss.w < win_surf->h)
                    x_vault += event.motion.xrel;
                break;
			case SDL_MOUSEBUTTONDOWN:
				if(start)
                {
                    ball.vx = 1.0;
                    ball.vy = 1.4;
                }
				start = false;
				break;
			default: break;
			}
		}
		prev = now;
		now = SDL_GetPerformanceCounter();
		delta_t = (double)((now - prev) * 500 / (double)SDL_GetPerformanceFrequency());
		draw();
	}
    SDL_Quit();
    return 0;
}


/*INITIALIZE THE GAME*/
void init()
{
    srand(time(NULL));
    int r = rand()%4 + 1;
    SDL_SetRelativeMouseMode(SDL_TRUE);
	pWindow = SDL_CreateWindow("Arkanoid", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 600, 600, SDL_WINDOW_SHOWN);

	win_surf = SDL_GetWindowSurface(pWindow);

	/*LOADING SPRITES*/
	plancheSprites = SDL_LoadBMP("./Arkanoid_sprites.bmp");
	SDL_SetColorKey(plancheSprites, true, 0);


	/*LOADING LEVEL*/
	if(initMap(r) != 1)
        printf("ERREUR DE CHARGEMENT");

    /*LOADING BALL*/
	initBall();
}

/*PLACES THE BRICKS ON THE LEVEL*/
int initMap(int level)
{
    int i = 0, col = 0, lig = 0;
    char currentChar;
    FILE *mapLevel;

    /*SELECT THE LEVEL*/
    switch(level)
    {
    case 1:
        mapLevel=fopen("levels/l1.txt","r");
        break;

    case 2:
        mapLevel=fopen("levels/l2.txt","r");
        break;

    case 3:
        mapLevel=fopen("levels/l3.txt","r");
        break;

    case 4:
        mapLevel=fopen("levels/l4.txt","r");
        break;
    }


    if(mapLevel)
    {
        do
        {
            currentChar = fgetc(mapLevel);
            listBrick[i] = currentChar;
            i++;
        }
        while(currentChar != EOF);

        fclose(mapLevel);
    }
    else
        return -1;

    return 1;
}



/*UPDTATES THE GAME SURFACE*/
void draw()
{
    if(start)
        ball.x = (x_vault + (srcVaiss.w / 2) - 10);

	/*BACKGROUND DISPLAY*/
	SDL_Rect dest = { 0, 0, 0, 0 };
	for (int j = 0; j < win_surf->h; j+=52)
		for (int i = 0; i < win_surf->w; i += 63)
		{
			dest.x = i;
			dest.y = j;
			SDL_BlitSurface(plancheSprites, &srcBg, win_surf, &dest);
		}

	/*BALL DISPLAY*/
	SDL_Rect dstBall = {ball.x, ball.y, 0, 0};
	SDL_BlitSurface(plancheSprites, &srcBall, win_surf, &dstBall);

	/*BRICK DISPLAY*/
	int col = 0;
    int lig = 0;
	for (int i=0; i<741; i++)
    {
        SDL_Rect dstBrick = {col, lig, 32, 16};
        if(listBrick[i] == '1')
            SDL_BlitSurface(plancheSprites, &srcBrick_Green, SDL_GetWindowSurface(pWindow), &dstBrick);
        if(listBrick[i] == '2')
            SDL_BlitSurface(plancheSprites, &srcBrick_Blue, SDL_GetWindowSurface(pWindow), &dstBrick);

        /*COLLISION BETWEEN BALL AND BRICK*/
        if(ball.y >= lig && ball.y <= lig + 16 &&
           ball.x >= col && ball.x <= col + 32 && listBrick[i] != '\n')
        {
            /*DETECTS WHERE COLISION TAKE PLACE*/
            if (listBrick[i] != '-')
            {
                if((ball.x >= col && ball.x <= col + 1)|| (ball.x >= col + 31 && ball.x <= col + 32))
                    {
                        ball.vx *= -1;
                        listBrick[i] = ('-');
                    }

                if((ball.y >= lig && ball.y <= lig+1)|| (ball.y >= lig + 15 && ball.y <= lig + 16) && listBrick[i] != '-')
                    ball.vy *= -1;

                listBrick[i] = ('-');
            }
        }

        col += 32;
        if(listBrick[i] == '\n')
        {
            col = 0;
            lig += 16;
        }
    }

	/*BALL MOVEMENT*/
	ball.x += ball.vx / delta_t;
	ball.y += ball.vy / delta_t;

	/*COLLISION BETWEEN BALL AND BORDERS*/
	if ((ball.x < 1) || (ball.x > (win_surf->w - 25)))
		ball.vx *= -1;
	if ((ball.y < 1) || (ball.y > (win_surf->h - 25)))
		ball.vy *= -1;

	/*COLLISION BETWEEN BALL AND BOTTOM*/
	if (ball.y >(win_surf->h - 25))
    {
        initBall();
        vie--;
    }

    /*PLAYER'S LIFES*/
    SDL_Rect vie1 = { 0, 0, 0, 0 };
	SDL_Rect vie2 = { 0, 0, 0, 0 };
	SDL_Rect vie3 = { 0, 0, 0, 0 };
	SDL_Rect vie4 = { 0, 0, 0, 0 };
	SDL_Rect vie5 = { 0, 0, 0, 0 };

    /*DISPLAY OF PLAYER'S LIFES*/
	switch(vie)
    {
        case 0 :
            vie1.x = -100;
            vie1.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie1);
            vie2.x = -100;
            vie2.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie2);
            vie3.x = -100;
            vie3.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie3);
            vie4.x = -100;
            vie4.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie4);
            vie5.x = -100;
            vie5.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie5);
            quit = true;
            break;
        case 1 :
            vie1.x = 10;
            vie1.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie1);
            vie2.x = -100;
            vie2.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie2);
            vie3.x = -100;
            vie3.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie3);
            vie4.x = -100;
            vie4.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie4);
            vie5.x = -100;
            vie5.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie5);
            break;
        case 2 :
            vie1.x = 10;
            vie1.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie1);
            vie2.x = 40;
            vie2.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie2);
            vie3.x = -100;
            vie3.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie3);
            vie4.x = -100;
            vie4.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie4);
            vie5.x = -100;
            vie5.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie5);
            break;
        case 3 :
            vie1.x = 10;
            vie1.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie1);
            vie2.x = 40;
            vie2.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie2);
            vie3.x = 70;
            vie3.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie3);
            vie4.x = -100;
            vie4.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie4);
            vie5.x = -100;
            vie5.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie5);
            break;
        case 4 :
            vie1.x = 10;
            vie1.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie1);
            vie2.x = 40;
            vie2.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie2);
            vie3.x = 70;
            vie3.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie3);
            vie4.x = 100;
            vie4.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie4);
            vie5.x = -100;
            vie5.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie5);
            break;
        case 5 :
            vie1.x = 10;
            vie1.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie1);
            vie2.x = 40;
            vie2.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie2);
            vie3.x = 70;
            vie3.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie3);
            vie4.x = 100;
            vie4.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie4);
            vie5.x = 130;
            vie5.y = win_surf->h - 30;
            SDL_BlitSurface(plancheSprites, &srcVie, win_surf, &vie5);
            break;
    }

	/*INITIALIZE VESSEL*/
	SDL_Rect vaiss = { 0, 0, 0, 0 };
	vaiss.x = x_vault;
	vaiss.y = win_surf->h - 32;
	SDL_BlitSurface(plancheSprites, &srcVaiss, win_surf, &vaiss);

	/*COLLISION BETWEEN VESSEL AND BALL*/
	int moitieVaiss = (vaiss.x + vaiss.w) / 2;

    if(ball.y >= vaiss.y - vaiss.h)
    {
        if(ball.x >= vaiss.x && ball.x <= vaiss.x + vaiss.w)
        {
            if(ball.x < moitieVaiss)
            {
                if(ball.x < moitieVaiss / 2)
                {
                    if(ball.vx > 0)
                        ball.vx *= -1.2;
                    else
                        ball.vx *= 1.2;
                    ball.vy *= -1.0;
                }
                else
                {
                    if(ball.vx > 0)
                        ball.vx *= -1.1;
                    else
                        ball.vx *= 1.1;
                    ball.vy *= -1.0;
                }
            }
            else
            {
                if(ball.x > moitieVaiss + (moitieVaiss / 2))
                {
                    if(ball.vx > 0)
                        ball.vx *= 1.2;
                    else
                        ball.vx *= -1.2;
                    ball.vy *= -1.0;
                }
                else
                {
                    if(ball.vx > 0)
                        ball.vx *= 1.1;
                    else
                        ball.vx *= -1.1;
                    ball.vy *= -1.0;
                }
            }
        }
    }

    if(ball.vx > 4)
    {
        ball.vx /= 2;
        ball.vy /= 2;
    }

    SDL_UpdateWindowSurface(pWindow);
}

/*GET THE BALL IN THE VESSEL*/
void initBall()
{
    ball.x = srcVaiss.x + (srcVaiss.w / 2);
	ball.y = win_surf->h - 32 - srcVaiss.h;
	ball.vx = 0;
	ball.vy = 0;
	start = true;
}

